import unittest
import main

class Tests(unittest.TestCase):
    def test_api_response(self):
        result = main.get_prices_from_url(12147800, mocked=False)
        self.assertIsInstance(result, dict, "should be an instance of class 'dict'")
        self.assertIn("data", result, "response should contain prices")
        self.assertIn("prices", result["data"], "response should contain prices")


    def test_prices_handling(self):
        expected_result = {
                          "F00104": {
                              "Name": "Super E5",
                              "price": "1.23"
                          },
                          "F00113": {
                              "Name": "Super E10",
                              "price": "2.34"
                          },
                          "F00125": {
                              "Name": "Ultimate 102",
                              "price": "3.45"
                          },
                          "F00400": {
                              "Name": "Diesel",
                              "price": "4.56"
                          },
                          "F00426": {
                              "Name": "Ultimate Diesel",
                              "price": "5.67"
                          }
                      }
        self.assertEqual(main.get_station_prices(12147800, '*'),expected_result, "should be the expected mocked result")
        

    def test_main_without_params(self):
        expected_result = {
                          12147800: {
                              "F00104": {
                                  "Name": "Super E5",
                                  "price": "1.23"
                              },
                              "F00113": {
                                  "Name": "Super E10",
                                  "price": "2.34"
                              },
                              "F00125": {
                                  "Name": "Ultimate 102",
                                  "price": "3.45"
                              },
                              "F00400": {
                                  "Name": "Diesel",
                                  "price": "4.56"
                              },
                              "F00426": {
                                  "Name": "Ultimate Diesel",
                                  "price": "5.67"
                              }
                          }
                      }
        self.assertEqual(main.main(), expected_result, "should be the expected mocked result")

    
if __name__ == '__main__':
    unittest.main()
