#!/usr/local/bin/python3

import argparse
import json
import logging
import requests
import sys

global_mocked = True


def parse_args(args):
    parser = argparse.ArgumentParser(description='Get gas station prices')
    parser.add_argument('-s', '--station', type=int, action='append')
    parser.add_argument('-f', '--fuel', action='append')
    parser.add_argument('-v', '--verbose', action='count')
    parser.add_argument('-m', '--mocked', action='store_true')
    parser.add_argument('-t', '--test', action='store_true')
    
    return parser.parse_args()


def get_prices_from_url(station, mocked = True):
    # ToDo: There is an option to configure mocking for unittest, see https://docs.python.org/3/library/unittest.mock.html
    if mocked == False or global_mocked == False:
        api_url = f'https://api.tankstelle.aral.de/api/v3/stations/{station}/prices'
        logging.debug("Api-URL:"+api_url)
        response = requests.get(api_url)
        logging.debug("API-Response Code:"+str(response))
        logging.debug("API-Response JSON:"+str(response.json()))
        return response.json()
    else:
        response = {
                'data': {
                    'prices': {
                        'F00104': '1.23',
                        'F00113': '2.34',
                        'F00125': '3.45',
                        'F00400': '4.56',
                        'F00426': '5.67'
                        },
                    'last_price_update': '2023-05-28T20:18:00+02:00'
                    }
                }
        return response


def get_station_prices(station, fuels):
    ALL_FUELS = {
            'F00104': "Super E5",
            'F00113': "Super E10",
            'F00125': "Ultimate 102",
            'F00400': "Diesel",
            'F00426': "Ultimate Diesel"
            }
    if fuels == '*':
        fuels = ALL_FUELS.keys()

    result = dict()

    response = get_prices_from_url(station)
    prices = response['data']['prices']
    for fuel in fuels:
        result[fuel] = {
                'Name': ALL_FUELS[fuel],
                'price': prices[fuel]
                }
    return result


def main(verbose=None, stations=None, fuels=None, mocked=True):

    global global_mocked

    if verbose is None:
        pass
    elif verbose == 1:
        logging.basicConfig(level=logging.INFO)
    elif verbose >= 2:
        logging.basicConfig(level=logging.DEBUG)
    
    logging.debug(f"Command Line Arguments: verbose: {verbose}, stations: {stations}, fuels: {fuels}, mocked: {mocked}")

    if mocked == False:
        global_mocked = False

    prices = dict()
    if stations is None:
        logging.warning("no station set, using Default")
        prices[12147800] = None
    else:
        for station in stations:
            prices[station] = None
    
    for station in prices.keys():
        if fuels is None:
            logging.warning("no fuels set, using Default")
            prices[station] = get_station_prices(station=station, fuels='*')
        else:
            prices[station] = get_station_prices(station=station, fuels=fuels)
    
    logging.info(json.dumps(prices,indent=4))
    return prices


if __name__ == "__main__":
    args = parse_args(sys.argv[1:])
    if args.test:
        import unittest
        suite = unittest.TestLoader().discover(".")
        unittest.TextTestRunner().run(suite)
    else:
        main(verbose=args.verbose, stations=args.station, fuels=args.fuel, mocked=args.mocked)
